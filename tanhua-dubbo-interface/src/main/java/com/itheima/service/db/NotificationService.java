package com.itheima.service.db;

import com.itheima.domain.db.Notification;

public interface NotificationService {
    //根据userId获取查询推送通知
    Notification findByUserId(Long userId);

    //设置推送通知选项
    void save(Notification notification);

    //修改推送通知选项
    void update(Notification notification);
}
