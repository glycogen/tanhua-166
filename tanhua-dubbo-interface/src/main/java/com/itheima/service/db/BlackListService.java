package com.itheima.service.db;

import com.itheima.vo.PageBeanVo;

public interface BlackListService {
    //黑名单分页查询
    PageBeanVo findByPage(Integer pageNum, Integer pageSize, Long userId);

    //删除黑名单
    void delete(Long userId, Long blackUserId);
}
