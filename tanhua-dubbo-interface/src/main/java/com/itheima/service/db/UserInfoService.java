package com.itheima.service.db;

import com.itheima.vo.UserInfo;

public interface UserInfoService {
    //保存用户信息
    void saveUserInfo(UserInfo userInfo);

    //更新用户信息
    void update(UserInfo userInfo);

    //查询用户信息
    UserInfo findById(Long userId);
}
