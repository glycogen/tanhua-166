package com.itheima.service.db;

import com.itheima.domain.db.User;

public interface UserService {
    //1.保存用户返回id
    long save(User user);
    //2.根据手机号查询
    User findByPhone(String phone);
}
