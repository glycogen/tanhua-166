package com.itheima.service.db;

import com.itheima.domain.db.Question;

public interface QuestionService {
    //根据userId陌生人问题
    Question findByUserId(Long userId);

    //保存陌生人问题
    void saveQuestion(Question question);

    //修改陌生人问题
    void updateQuestion(Question question);
}
