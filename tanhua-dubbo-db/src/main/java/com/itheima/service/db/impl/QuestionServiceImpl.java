package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Question;
import com.itheima.mapper.QuestionMapper;
import com.itheima.service.db.QuestionService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionMapper questionMapper;

    @Override
    public Question findByUserId(Long userId) {
        //构建条件
        QueryWrapper<Question> qw = new QueryWrapper<>();
        qw.eq("user_id", userId);
        //查询一条
        return questionMapper.selectOne(qw);

    }
    //设置陌生人问题
    @Override
    public void saveQuestion(Question question) {
        questionMapper.insert(question);
    }

    //修改陌生人问题
    @Override
    public void updateQuestion(Question question) {
        questionMapper.updateById(question);
    }
}
