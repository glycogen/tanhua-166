package com.itheima.service.db.impl;

import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.User;
import com.itheima.mapper.UserMapper;
import com.itheima.service.db.UserService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

@DubboService
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public long save(User user) {

        //完善数据
        String md5Pwd = SecureUtil.md5(user.getPassword());
        user.setPassword(md5Pwd);
//        user.setCreated(new Date());
//        user.setUpdated(new Date());

        //保存方法
       userMapper.insert(user);
        return user.getId();
    }

    @Override
    public User findByPhone(String phone) {
        //编写条件
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("mobile", phone);

        return userMapper.selectOne(qw);
    }
}
