package com.itheima.service.db.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.domain.db.Notification;
import com.itheima.domain.db.Question;
import com.itheima.mapper.NotificationMapper;
import com.itheima.mapper.QuestionMapper;
import com.itheima.service.db.NotificationService;
import com.itheima.service.db.QuestionService;
import org.apache.dubbo.config.annotation.DubboService;
import org.springframework.beans.factory.annotation.Autowired;

@DubboService
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    private NotificationMapper notificationMapper;

    @Override
    public Notification findByUserId(Long userId) {
        //构建条件
        QueryWrapper<Notification> qw = new QueryWrapper<>();
        qw.eq("user_id", userId);
        //查询一条
       return notificationMapper.selectOne(qw);
    }

    //新增推送设置
    @Override
    public void save(Notification notification) {
        notificationMapper.insert(notification);
    }
    //修改推送设置
    @Override
    public void update(Notification notification) {
        notificationMapper.updateById(notification);
    }
}
