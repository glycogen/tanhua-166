package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.vo.UserInfo;

//用户详情
public interface UserInfoMapper extends BaseMapper<UserInfo> {
}