package com.itheima.app.test;

import cn.hutool.core.io.FileUtil;
import com.itheima.autoconfig.face.AipFaceTemplate;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AipTest {

    @Autowired
    private AipFaceTemplate aipFaceTemplate;

    @Test
    public void testAipFace(){
        String filename = "E:\\yingyong\\pic\\1637827630436.jpg";
        File file = new File(filename);
        byte[] bytes = FileUtil.readBytes(file);
        boolean detect = aipFaceTemplate.detect(bytes);
        System.err.println(detect);
    }
}
