package com.itheima.app.interceptor;

import com.itheima.domain.db.User;

//线程内存储user对象
public class UserHolder {
    private static final ThreadLocal<User> TLUser = new ThreadLocal<>();

    //设置
        public static void set(User user){
            TLUser.set(user);
        }
    //读取
        public static User get(){
            return TLUser.get();
        }
    //移除
        public static void remove(){
            TLUser.remove();
        }
}
