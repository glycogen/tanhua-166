package com.itheima.app.controller;

import com.itheima.domain.db.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
public class SettingController {
    @Autowired
    private SettingManager settingManager;

    //获取配置信息
    @GetMapping("/users/settings")
    public ResponseEntity findSettingVo(){
       return settingManager.findSettingVo();
    }

    //设置配置信息
    @PostMapping("/users/questions")
    public void setQuestion(@RequestBody Map<String, String> param) {
        String content = param.get("content");

        settingManager.setQuestion(content);
    }

    //设置推送设置
    @PostMapping("/users/notifications/setting")
    public void setNotification(@RequestBody Notification param) {
        settingManager.setNotification(param);
    }

    //黑名单查询
    @GetMapping("/users/blacklist")
    public ResponseEntity findBlackListByPage(
            @RequestParam(value = "page",defaultValue = "1") Integer pageNum,
            @RequestParam(value = "pagesize",defaultValue = "10") Integer pageSize)
    {
        return settingManager.findBlackListByPage(pageNum,pageSize);
    }

    //移除黑名单
    @DeleteMapping("/users/blacklist/{blackUserId}")
    public void delete(@PathVariable Long blackUserId) {
        settingManager.deleteBlackList(blackUserId);
    }
}
