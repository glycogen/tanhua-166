package com.itheima.app.controller;

import cn.hutool.setting.Setting;
import com.itheima.app.interceptor.UserHolder;
import com.itheima.domain.db.BlackList;
import com.itheima.domain.db.Notification;
import com.itheima.domain.db.Question;
import com.itheima.domain.db.User;
import com.itheima.service.db.BlackListService;
import com.itheima.service.db.NotificationService;
import com.itheima.service.db.QuestionService;
import com.itheima.service.db.UserInfoService;
import com.itheima.vo.PageBeanVo;
import com.itheima.vo.SettingVo;
import com.itheima.vo.UserInfo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class SettingManager {

    @DubboReference
    private QuestionService questionService;

    @DubboReference
    private NotificationService notificationService;

    public ResponseEntity findSettingVo() {

        //获取线程内userId
        User user = UserHolder.get();
        //根据userId查询陌生人问题
        Question question = questionService.findByUserId(user.getId());
        //根据userId查询推送通知
        Notification notification = notificationService.findByUserId(user.getId());
        //封装vo
        SettingVo vo = new SettingVo();
        vo.setId(user.getId());//用户ID
        vo.setPhone(user.getMobile());//用户手机号
        if (question!=null) {
            vo.setStrangerQuestion(question.getStrangerQuestion());
        }
        if (notification != null) {
            vo.setGonggaoNotification(notification.getGonggaoNotification());
            vo.setPinglunNotification(notification.getPinglunNotification());
            vo.setLikeNotification(notification.getLikeNotification());
        }

        //返回vo
        return ResponseEntity.ok(vo);


    }

    public void setQuestion(String content) {
        //获取线程内userId
        User user = UserHolder.get();
        //根据userId查询陌生人问题
        Question question = questionService.findByUserId(user.getId());
        //判断
        if (question == null) {
            //没有陌生人问题，新增
            question = new Question();
            //设置用户id
            question.setUserId(user.getId());
            //设置陌生人问题
            question.setStrangerQuestion(content);
            //保存
            questionService.saveQuestion(question);
        } else {
            //有陌生人问题 修改
            //设置陌生人问题
            question.setStrangerQuestion(content);
            //修改
            questionService.updateQuestion(question);

        } 


    }

    //设置推送设置
    public void setNotification(Notification param) {
        //获取线程内userId
        Long userId = UserHolder.get().getId();
        //2. 根据UserId查询推送通知
        Notification notification = notificationService.findByUserId(userId);

        //判断
        if (notification == null) {
            //没有查询到推送通知
            param.setUserId(userId);
            //保存
            notificationService.save(param);
        } else {
            //查询到了
            notification.setGonggaoNotification(param.getGonggaoNotification());
            notification.setLikeNotification(param.getLikeNotification());
            notification.setPinglunNotification(param.getPinglunNotification());
            notificationService.update(notification);
        }
    }


    @DubboReference
    private BlackListService blackListService;

    @DubboReference
    private UserInfoService userInfoService;

    //设置黑名单分页查询
    public ResponseEntity findBlackListByPage(Integer pageNum, Integer pageSize) {
        //1. 获取线程内userId
        Long userId = UserHolder.get().getId();

        //2. 根据条件查询黑名单分页对象
        PageBeanVo pageBeanVo = blackListService.findByPage(pageNum, pageSize, userId);

        //3. 根据blackUserId查询userInfo
        //3-1. 获取黑名单集合
        List<BlackList> blackList = (List<BlackList>) pageBeanVo.getItems();
        //3-2. 声明userInFo集合
        List<UserInfo> userInfoList = new ArrayList<>();
        //3-3. 遍历黑名单集合
        if (blackList!=null && blackList.size()>0) {
            for (BlackList list : blackList) {
                //获取黑名单用户id
                Long blackUserId = list.getBlackUserId();
                //查询userInfo
                UserInfo userInfo = userInfoService.findById(blackUserId);
                //添加到集合
                userInfoList.add(userInfo);
            }
        }
        //4. 将userInfo封装到分页对象
        pageBeanVo.setItems(userInfoList);

        return ResponseEntity.ok(pageBeanVo);
    }

    public void deleteBlackList(Long blackUserId) {
        //获取线程内userId
        Long userId = UserHolder.get().getId();

        //调用rpc删除
        blackListService.delete(userId, blackUserId);
    }
}
