package com.itheima.app.exception;

import com.itheima.vo.ErrorResult;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;

@ControllerAdvice //全局异常处理器
public class GlobalExceptionAdvice {

    public ResponseEntity exceptionHandlerMethod(Exception ex) {
        //将异常信息输出到控制台
        ex.printStackTrace();
        //友情提示
        return ResponseEntity.status(500).body(ErrorResult.error());
    }

}
